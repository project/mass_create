<?php

/**
 * @file .admin.inc
 * Functions that are only called on the admin pages.
 */

/**
 * Module settings form.
 */
function mass_create_settings() {

  $form['mass_create_info'] = array(
    '#value' => t('<p>The  module allows you to create mass fake content from a list.</p>')
    );

  return system_settings_form($form);
}

/**
 * Function to massively create content.
 */
function mass_create_submit($form, &$form_state) {

	global $user;

	// we prepare return arrays
	$added = array();
	$invalid = array();

	// we get values
  	$titles = preg_split("/\n/", $form_state['values']['titles']);
  	$type = $form_state['values']['type'];
  	$body = $form_state['values']['body'];
  	$term_id = $form_state['values']['term_id'];
	$menu_parent= $form_state['values']['menu_parent'];
	$create_menu_parent= $form_state['values']['create_menu_parent'];
	
  	// we parse the titles list
  	foreach ($titles as $title) {
	    $title = trim($title);
	    $title=(string) $title;
	    if ($title!="") {

			// we create an empty object
	    	$node = new StdClass();
	    	// then we fill the node attributes
			$node->type = $type;
			$node->status = 1;
			$node->title = $title;
			$node->body = $body;
			// owner
			$nodenew->uid = $user->uid;
			// terms
			$node->taxonomy = array($term_id);
			
			// menu
			if ($create_menu_parent==1){
				list($menu_name, $plid) = explode(':', $form_state['values']['menu_parent']);
				
			    $node->menu = array(
			    'link_title' => $title,
			    'mlid' => 0,
			    'plid' => $plid,
			    'menu_name' => $menu_name,
			    'weight' => 0,
			    'options' => array(),
			    'module' => 'menu',
			    'expanded' => 0,
			    'hidden' => 0,
			    'has_children' => 0,
			    'customized' => 0); 
			}
		    
			// we save the node
			node_save($node);

	    }
	}
}

/**
 * Function to display mass create form
 */
function mass_create() {

  global $language;

  
  // Type settings
  $select = array();
  $types = node_get_types();
  $names = node_get_types('names');
  foreach ($types as $key => $type) {
    $select[$key] = $names[$key];
  } 
  $form['type'] = array(
    '#title' => t('Type to create'),
    '#type' => 'select',
    '#options' => $select,
    '#description' => t('Select here which node types you want to create.'),
    '#prefix' => '<div class="mass_create_select">',
    '#suffix' => '</div>',
    );   
  
  $form['titles'] = array(
    '#type' => 'textarea',
    '#title' => t('Node titles'),
    '#cols' => 60,
    '#rows' => 5,
    '#description' => t('Titles must be separated by newline.'),
  );
  
  $form['body'] = array(
    '#type' => 'textarea',
    '#title' => t('Content'),
    '#cols' => 60,
    '#rows' => 5,
    '#description' => t('Define here what content will be set into the nodes.'),
  );
  
  $form['term_id'] = array(
     '#type' => 'select',
     '#title' => t("Term"),
     '#options' => taxonomy_form_all(),
     '#multiple' => FALSE,
     '#size' => 10,
     '#theme' => 'taxonomy_term_select'
  );
  
  // Generate a list of possible parents (not including this item or descendants).
  $form['create_menu_parent'] = array(
    '#type' => 'checkbox',
    '#title' => t('Appear in a menu?'),
    '#description' => t('You can choose wheither or not the nodes will appear as menu items.'),
  );
  $options = menu_parent_options(menu_get_menus(), null);
  if (!isset($options[$default])) {
    $default = 'navigation:0';
  }
  $form['menu_parent'] = array(
    '#type' => 'select',
    '#title' => t('Parent menu item'),
    '#options' => $options,
    '#description' => t('You can choose a parent menu item where the nodes will appear as menu items.'),
    '#attributes' => array('class' => 'menu-title-select'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create'),
  );
  return $form;
}